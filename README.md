This is a little demo of an image rollover effect.
It's based on JDK8 which means you have to adjust the lambdas if you would like to use it on JDK7.

You will find two rollover effects in the class and you can apply them by calling either addRotateXY() or addRotateX().
